(revised on 2017-01-19 by Stefano Salsano)

Per eseguire l'applicazione, scrivere sulla linea di comando: python compile.py.

Il file compile.py apre il file json che viene generato dopo aver effettuato il deploy dell'applicazione (il file nella cartella /tmp) [ora è settato un file "data.json" di prova], lo apre e ne legge il contenuto.

Il contenuto viene elaborato e salvato in una nuova pagina json ("server-client-final.json") dove si trovano i client/server e tutte le informazioni quali: tipo di connessione, protocollo, porte, indirizzi IP, tempo di esecuzione ecc.

Il file "server-client.json" è lo "scheletro" del file "server-client-final.json".

Una volta elaborati e scritti i dati nel file json, il file compile.py passa all'esecuzione dei file server.py e client.py che generano il codice Iperf.
Leggono tutti i dati memorizzati precedentemente nel file server-client-final.json, li inseriscono nelle rispettive stringhe insieme ai comandi esatti del codice che genera la connessione e il trasferimento dati, e procedono all'esecuzione dello scambio di pacchetti.