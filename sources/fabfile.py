import json
import time
import datetime
import os
from fabric.api import *


@task
@parallel
def client_generator(client_list):
    client_obj = client_list[env.host]
    ip_addr = client_obj["ipdest"]
    dest_port = client_obj["dport"]
    conn = client_obj["prot"]
    duration = int(client_obj["end"]) - int(client_obj["start"])
    bandwidth = client_obj["rate"]
    time_sleep = int(client_obj["start"])
    iperf_command = "iperf -c " + ip_addr + " -p " + dest_port + " -t " + str(
        duration) + " -b " + bandwidth + "m "
    if conn == "udp":
        iperf_command = iperf_command + " -u "
    i = datetime.datetime.now()
    log_file_name = '/home/user/PycharmProjects/IperfGenerator/logs/cli-' + env.host + '-' + str(i) + '.json'
    startlog(log_file_name)
    time.sleep(time_sleep)
    str_log = run(iperf_command).stdout
    log(log_file_name, str_log)


@task
def generate_clients():
    client_list = {}
    client_addr_list = []
    for clientName in data["clients"].keys():
        client_obj = data["clients"][clientName]
        loopbackIpAddr = client_obj["mgtIP"]
        client_addr_list.append(loopbackIpAddr)
        client_list[loopbackIpAddr] = client_obj
    execute(client_generator, client_list, hosts=client_addr_list)


@task
@parallel
def server_generator(command_dict):
    i = datetime.datetime.now()
    log_file_name = '/home/user/PycharmProjects/IperfGenerator/logs/ser-'+env.host+'-'+str(i)+'.json'
    startlog(log_file_name)
    str_log = run(command_dict[env.host]).stdout
    log(log_file_name, str_log)


@task
def generate_servers():
    host_list = []
    command_list = {}
    for serverName in servers_to_mount.keys():
        curr_mgt_ip = servers_to_mount[serverName]["mgtIp"]
        command_list[curr_mgt_ip] = construct_iperf_command(serverName)
        host_list.append(curr_mgt_ip)
    execute(server_generator, command_list, hosts=host_list)


def construct_iperf_command(server_name):
    conn = servers_to_mount[server_name]["prot"]
    iperf_command = "iperf -s "
    if conn == "udp":
        iperf_command = iperf_command + " -u "
    elif conn != "tcp":
        print("Invalid input!")
        exit()
    port = servers_to_mount[server_name]["port"]
    iperf_command = iperf_command + " -p " + port
    return iperf_command


def startlog(logFileName):
    logfile = open(logFileName, "a+")
    logfile.close()


def log(logFileName, msg):
    logfile = open(logFileName, "a+")
    logfile.write(msg + "\n")
    logfile.close()


env.user = os.getenv('SSH_USER', 'root')
env.password = os.getenv('SSH_PASSWORD', 'root')
json_data = open("iperfGen.json").read()
data = json.loads(json_data)
servers_to_mount = data["servers"]