import json


json_data = open("deploymentDescriptor.json").read()
json_data1 = open("trafficModel.json").read()
iperfDirectiveFile = open('/home/user/PycharmProjects/IperfGenerator/iperfGen.json', 'w')

generatedTopology = json.loads(json_data)
flowDirectiveDS = json.loads(json_data1)
iperfDirectiveDS = flowDirectiveDS

# Each server on the flowDirectiveDataStructure receives an ip address:
serversToAssign = flowDirectiveDS["servers"]
for serverName in serversToAssign.keys():
    ipAddr = generatedTopology[serverName]["interfaces"][serverName + "-" + serversToAssign[serverName]["interface"]][
        "ip"]
    mgtAddr = generatedTopology[serverName]["mgt_IP"]
    ipAddrWithoutMask = ipAddr.split("/")[0]
    iperfDirectiveDS["servers"][serverName]["ip"] = ipAddrWithoutMask
    iperfDirectiveDS["servers"][serverName]["mgtIp"] = mgtAddr

# Each Client on the flowDirectiveDataStructure gets a destIp corresponding to its Dest Name.
clientsToAssign = flowDirectiveDS["clients"]
for clientName in clientsToAssign.keys():
    serverName = clientsToAssign[clientName]["dest"]
    ipAddr = generatedTopology[serverName]["interfaces"][serverName + "-" + serversToAssign[serverName]["interface"]][
        "ip"]
    ipAddrWithoutMask = ipAddr.split("/")[0]
    iperfDirectiveDS["clients"][clientName]["ipdest"] = ipAddrWithoutMask
    mgtIpAddr = generatedTopology[clientName]["mgt_IP"]
    iperfDirectiveDS["clients"][clientName]["mgtIP"] = mgtIpAddr

iperfDirectiveString = json.dumps(iperfDirectiveDS)
iperfDirectiveFile.write(str(iperfDirectiveString))
iperfDirectiveFile.close()
