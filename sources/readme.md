# IperfGen RDCL-3D plugin

Simple Iperf experiments on a RDCL-3D deployed-topology.

## Installation

Requires python 2.6-2.7.

$ virtualenv --python=/path/to/python2.6 <path/to/myvirtualenv>

Requires fabric

pip install fabric

## Usage

Supposing we have a deployed RDCL-3D topology alive, and supposing the
deployment descriptor of such topology is in deploymentDescriptor.json

python InilializeIperGen.py

Parses the trafficModel.json iperf directives file. (User changes the iperf flows editing this file)

fab generate_servers

Starts the tasks for all the servers enlisted on trafficModel.json

fab generate_clients

Starts the clients according to trafficModel.json


## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license